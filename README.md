composer.json
    "require": {
        "php": "^7.2.5",
        "drhttp/moesif-laravel": "^1.4.8",
    }



kernel.php

    protected $middleware = [
        \Moesif\Middleware\MoesifLaravel::class,


config/moesif.php


<?php
return [
    'applicationId' => parse_url(getenv('DRHTTP_DSN'), PHP_URL_USER),
];
?>



This is the official [PHP](https://www.php.net/) ([Laravel](https://laravel.com/)) client for [Dr. Ashtetepe](https://drhttp.com/) service.

*Note: At the moment, as we are developing Dr. Ashtetepe, we ask you to use [Moesif](https://www.moesif.com/)'s [php library](https://github.com/Moesif/moesifapi-php). Thanks to them to permit that via their [Apache licence](https://raw.githubusercontent.com/Moesif/moesifapi-php/master/LICENSE)*. This README is a [TL;DR](https://en.wiktionary.org/wiki/tl;dr) for Dr.Ashtetepe usage.

# Installation

 1) Install package with `composer` (or any compatible package manager). 
    ```
    composer require drhttp/moesif-laravel
    ```

 2) You will need a `dsn` ([Data source name](https://en.wikipedia.org/wiki/Data_source_name)) which can be found in [your project](https://drhttp.com/projects). eg: `https://<my_project_api_key>@api.drhttp.com/`

# Usage for [Laravel](https://laravel.com/)

[An integration example is provided here](https://bitbucket.org/drhttp/drhttp-php/src/master/examples/laravel/)

## Inbound request recording

1) Enable the middleware in `App/Http/Kernel.php` :
    ```php
    protected $middleware = [
        ...
        \Moesif\Middleware\MoesifLaravel::class,
    ];

    ```

1) Configure `config/moesif.php` :
    ```php
    <?php
        return [
            'applicationId' => '<my_project_api_key>',
        ];
    ?>
    ```

### User identification

See [identifiUserId in Moesif documentation](https://github.com/Moesif/moesif-laravel#identifyuserid)

### Device identification

*Note: Device identification is not available yet in the php library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the php library*

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-php/issues/new) you encounter concerning documentation or implementation. This is very much appreciated. We'll upstream the improvements to Moesif.
